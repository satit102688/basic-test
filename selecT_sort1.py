def selection_sort(data: list) -> list:
    sorted_data = data.copy()
    for i in range(data[0], data[-1]):
        maxI = 0
        for j in range(i, i + 1):
            if sorted_data[j] > sorted_data[maxI]:
                sorted_data[maxI], sorted_data[j] = sorted_data[j], sorted_data[maxI]

    return sorted_data


if __name__ == "__main__":
    data = list(map(int, input("enter 10 number: ").split()))

    sorted_data = selection_sort(data)

    print(sorted_data)
